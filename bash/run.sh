#!/bin/bash

cd ~
mkdir -p code
cd code
git clone https://gitlab.com/theotheu/hlc-provisioning
# just to make sure that latest commits are used
cd hlc-provisioning
git fetch --all && git reset --hard && git pull origin master
cd ~/hlc-provisioning/bash

curl -O https://hyperledger.github.io/composer/latest/prereqs-ubuntu.sh
chmod u+x prereqs-ubuntu.sh
./prereqs-ubuntu.sh

echo
echo ===========================================================================
echo ===========================================================================
echo = Now exiting this terminal session.
echo = You have to login again and continue with the next steps.
echo ===========================================================================
echo ===========================================================================
echo
echo

exit
