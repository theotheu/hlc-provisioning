#!/bin/bash
echo
echo See https://hyperledger.github.io/composer/latest/installing/development-tools
echo
echo
echo Starting and stopping Hyperledger Fabric
cd ~/fabric-dev-servers
./startFabric.sh
./createPeerAdminCard.sh
echo "Start the web app (Playground)"
composer-playground

echo Done!
echo "Point your browser to http://localhost:8080/login"
