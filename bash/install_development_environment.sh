#!/bin/bash

echo
echo See https://hyperledger.github.io/composer/latest/installing/development-tools
echo
echo
echo Step 1: Install the CLI tools
echo
npm install -g composer-cli
npm install -g composer-rest-server
npm install -g generator-hyperledger-composer
npm install -g yo
echo Step 2: Install Playground
npm install -g composer-playground
echo Step 3: Set up your IDE
echo --- Nothing todo on server
echo Step 4: Install Hyperledger Fabric
mkdir ~/fabric-dev-servers && cd ~/fabric-dev-servers

curl -O https://raw.githubusercontent.com/hyperledger/composer-tools/master/packages/fabric-dev-servers/fabric-dev-servers.tar.gz
tar -xvf fabric-dev-servers.tar.gz

cd ~/fabric-dev-servers
./downloadFabric.sh

./controlling_dev_environment.sh
