# Ansible provisioning

The vagrant directory is for local test purposes.

If you want to test local, start with vagrant. Then continue with ansible.

See README in vagrant directory and ansible directory.

## One-liner for ordinary user to run on new machine. The script asks for a root password. 


`wget -qO- https://gitlab.com/theotheu/hlc-provisioning/raw/master/run.sh | bash`



Iptables is applied in strict mode. See `/root/iptables.strict`. Use `/root/iptables.minial` for unsecure operations (not recommended).


# Provisioning from bash scripts
* `wget -qO- https://gitlab.com/theotheu/hlc-provisioning/raw/master/bash/run.sh | bash`
* `wget -qO- https://gitlab.com/theotheu/hlc-provisioning/raw/master/bash/install_development_environment.sh | bash`
* `wget -qO- https://gitlab.com/theotheu/hlc-provisioning/raw/master/bash/developer_tutorial_for_creating_hyperledger_composer_solution.sh | bash`
